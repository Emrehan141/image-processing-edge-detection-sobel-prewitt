﻿import re
import numpy
import cv2

#dosyadan resim okuyan fonksiyon
#girdi: imagename= okunacak dosyanın tam adı
#çıktı: resmin matris hali
def readImage(imagename):
    print(imagename+" isimli dosya okunuyor")
    with open(imagename, 'rb') as f:
        buffer=f.read()
    try:
        #resmin başındaki header kısmındaki bilgiler alınıyor
        header, width, height, maxval = re.search(
            b"(^P5\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n]\s)*)", buffer).groups()
    except AttributeError:
        raise ValueError("Error when read PGM image => '%s'" % imagename)
    return numpy.frombuffer(buffer,
                            dtype='u1',
                            count=int(width)*int(height),
                            offset=len(header)
                            ).reshape((int(height), int(width)))

#dosyadan resim yazan fonksiyon
#girdi: image:matris türünde resim, imagename= yazılacak dosyanın tam adı
def writeImage(image, imagename):

    #Aşağıdaki kısım resim kaydetmek için yazdığım fonksiyon.
    #Bu kısım readImage fonksiyonundan gelen değeri yazdırabilse de
    #Filtrelenmiş resimleri yazdıramıyor. Yani yarı yarıya çalışıyor.
    #Sorun oluşturmaması açısından yazma işlemini opencv ile de yaptım.
    with open(imagename, 'wb') as f:
        f.write(str.encode('P5\n{} {}\n{}\n'.format(image.shape[1], image.shape[0], numpy.asscalar(numpy.int16(image.max())))))
        image.tofile(f)

#verilen matrisle kenar belirleme işlemi yapan fonksiyon
#girdi: image:matris türünde resim, filtername: programın en alınta tanımlanan dizi
#girdi: 3x3 matris oluşturmak için 9 elemanlı
#çıktı: filtre uygulanmış resim
def edgeFilter (image, filtername) :
    print("Resime "+ ("sobel" if filtername is "sobel" else "prewitt") +" uygulanıyor")
    width = image.shape[1]
    height = image.shape[0]
    newImage = numpy.zeros(shape = (width , height))

    #tüm resim üzerinde 3x3'lük matris uygulanıyor.
    for i in range(1, width - 1):
        for j in range(1, height - 1):
            sum=0
            for ii in range (0,3):
                for jj in range (0,3):
                    sum += int(image[i-1+ii][j-1+jj])*int(filtername[1][(3*ii)+jj])
                    sum += int(image[i-1+ii][j-1+jj])*int(filtername[0][(3*ii)+jj])
            newImage[i][j] = int(sum if sum >= 0 and sum <= 255 else (0 if sum < 0 else 255))
            #çarpımlar toplamı yapılıyor. Çıkan sonuç 0'dan küçükse 0'a, 255'ten büyükse 255'e eşitleniyor.
    print("Resime " + ("sobel" if filtername is "sobel" else "prewitt") + " uygulandı")
    return [newImage]

#verilen T değeri ile ortalama bir değere yakınsayan, yakınsadığı bu değerin altındaki pixelleri siyah
#üstünde olanları beyaz yapan, binary resim yapmak için bir fonksiyon
#girdi: image:matris türünde resim, T: başlangıç değeri
#çıktı: binary bir resmin matris hali
def threshold(image, T):
    print("Resime threshold uygulanıyor")
    width = image.shape[1]
    height = image.shape[0]

    # g1 ve g2; ilk_eleman=toplamları ikinci_eleman=eleman sayısı
    g=[[0,0],[0,0]]

    #tüm pixeller dolaşılarak g1 ve g2 için toplam ve eleman sayısı hesaplanıyor
    for i in range(0, width - 1):
        for j in range(0, height - 1):
            g[0 if (image[i][j] < T) else 1][0]+=image[i][j]
            g[0 if (image[i][j] < T) else 1][1]+= 1

    #g1 ve g2 için m1 ve m2 ortalama hesaplanıyor. 0'a bölme problemini aşmak için if kontrolü var.
    m1=int(0 if g[0][1] is 0 else (g[0][0] / g[0][1]))
    m2=int(0 if g[1][1] is 0 else (g[1][0] / g[1][1]))
    newT = int(m1+m2)/2

    #T'nin değişimi 2'den küçük olana kadar fonksiyon tekrarlanıyor.
    if (int(newT)-T)>=2:
        threshold(image,newT)

    #2'den büyük olduğunda yeni bir matrise, yeni T değerinden büyük değerler beyaz,
    #küçük değerler siyah olarak atanıp döndürülüyor.
    newImage = numpy.zeros(shape = (width , height))
    for i in range(0, width - 1):
        for j in range(0, height - 1):
            newImage[i][j] = (255 if image[i][j] > newT else 0)
    print("Resime threshold uygulandı")
    return newImage

#varsayılan sobel ve prewitt dizisi. 9 elemanlı, 3x3 matris gibi kullanılıyor.
sobel=[[-1, -2, -1, 0, 0, 0, 1, 2, 1],[-1, 0, 1, -2, 0, 2, -1, 0, 1]]
prewitt=[[-1, -1, -1, 0, 0, 0, 1, 1, 1],[-1, 0, 1, -1, 0, 1, -1, 0, 1]]

#tüm resimler okunarak images isimle değişkene atanıyor.
#images = [readImage("house.256.pgm"), readImage("monarch.512.pgm"), readImage("pentagone.1024.pgm")]

writeImage(readImage("house.256.pgm"),"deneme.pgm")
#for i in range(0,10):
#    image = readImage("pgm/"+str(i)+".pgm")
#    sobelimage=edgeFilter(image,sobel).pop()
#    threshimage=threshold(sobelimage,128)
#    writeImage(sobelimage, "pgm2/" + str(i) + ".pgm")
#    writeImage(threshimage,"pgm2/"+str(i)+"+thresh.pgm")

#3 resim içinde sobel, sobel+threshold, prewitt, prewitt+threshold uygulanması
#for i in range(0,3):
#    sobelImage=edgeFilter(images[i],sobel).pop()
#    prewittImage=edgeFilter(images[i],prewitt).pop()
#
#    writeImage(sobelImage,str(i)+"/"+str(i)+".sobel.pgm")
#    writeImage(prewittImage,str(i)+"/"+str(i)+".prewitt.pgm")
#    writeImage(threshold(sobelImage,128),str(i)+"/"+str(i)+".sobel+threshold.pgm")
#    writeImage(threshold(prewittImage,128),str(i)+"/"+str(i)+".prewitt+threshold.pgm")